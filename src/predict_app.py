"""House price prediction service"""
from joblib import load

import yaml
from flask import Flask, request
from yaml import SafeLoader

app = Flask(__name__)
config = {
    'model_path': 'models/model_1.joblib'
}
model = config['model_path']


@app.route("/")
def home():
    return 'Housing price service. Use /predict endpoint'


@app.route("/predict")
def predict():
    """Dummy service"""
    f_1 = request.args.get("GrLivArea")
    f_2 = request.args.get("f2")
    f_3 = request.args.get("f3")
    f_1, f_2, f_3 = int(f_1), int(f_2), int(f_3)
    result = model.predict([[f_1]])[0]
    return f'{round(result / 1000)} тыс. руб.'


if __name__ == "__main__":
    app.run(host='0.0.0.0')
