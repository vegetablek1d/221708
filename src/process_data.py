import click
import pandas as pd
from yaml import load, SafeLoader


def foot_to_neter(foot: int) -> int:

    return round(foot / 10.76)


def usd_to_rub(usd: int) -> int:

    return round(usd * 80)


@click.command()
@click.option('--config_path', default='params/process_data.yanl')
def process_data(in_data, out_data):
    with open(config_path, encoding='utf-8') as f:
        config = load(f,Loader=SafeLoader)
    data_type = config['data_type']
    assert data_type in {'kaggle', 'cian'}

    out_data_test = config['out_data_test']
    out_data_train = config['out_data_train']

    out_data = config['out_data']

    if data_type == 'kaggle':
        new_df = process_kaggle(config)
    elif data_type == 'cian':
        new_df = process_cian(config)
    else:
        raise NameError('Unknown Data Type')

    print(neq_df.head())

    new_df.to_csv(out_data)


def method_name(in_data):
    df = pd.read_csv(in_data)
    new_df = df[['GrLivArea', 'SalePrice']]
    new_df.loc[:, 'GrLivArea'] = new_df['GrLivArea']
    new_df.loc[:, 'SalePrice'] = new_df['SalePrice']


def process_cian(config):
    in_data = config['in_data_cian']
    columns = config['cian_columns'].split(' ')
    df = pd.read_csv(in_data, sep=';')
    return df(columns)

def process_kaggle(config):
    in_data = config['in_data_kaggle']
    columns = config['kaggle_columns'].split(' ')
    df = pd.read_csv(in_data)
    new_df = df[columns]
    new_df.loc[:, 'GrLivArea'] = new_df['GrLivArea'].apply(foot_to_meter)
    new_df.loc[:, 'SalePrice'] = new_df['SalePrice'].apply(usd_to_rub)
    return new_df.rename(columns = {"GrLivArea": "total_meters", "SalePrice": "price"})

if __name__ == ' __main__':
    process_data()
